package svndumpfilter;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

public class ByteArrayFilterTest {

    private static String K13 = "svn:eol-style\nV 4\nCRLF\nK 13\n";
    private byte[] inputDump;
    Path resourceDirectory = Paths.get("src","test","resources");
    String absolutePath = resourceDirectory.toFile().getAbsolutePath();

    @Before
    public void setUp() throws Exception {
        inputDump = Files.readAllBytes(Paths.get(absolutePath + "/K13_dump_input.txt"));
    }

    @Test
    public void filterK13Only() throws IOException {
        // remove the sequence ending in K13, expected input has been validated by
        // svnadmin load into svn and the binaries tested to be correct
        ByteArrayFilter baf = new ByteArrayFilter(inputDump);
        byte [] filteredInput = baf.filter(K13.getBytes());
        byte [] expectedOutputDump = Files.readAllBytes(Paths.get(absolutePath +"/K13_removed_expected_output.txt"));

        assert Arrays.equals(filteredInput, expectedOutputDump);


    }


}