package svndumpfilter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

/**
 * This code is intended to remove properties from svn dump files.  The properties where
 * created erroneously by the pvcs2svn tool, and looks like this:
 *
 *     K 13
 *     vn:eol-style
 *     V 4
 *     CRLF
 *
 *
 * @author stevenlennon
 *
 */
public class Main {
    // TODO: Search for {STATIC_SEQUENCE\nK 13} then {STATIC_SEQUENCE}
    public static final String STATIC_SEQUENCE = "K 13\nsvn:eol-style\nV 4\nCRLF\n";
    public static final List<String> SEARCH_SEQUENCES = Arrays.asList(STATIC_SEQUENCE
//                                                                        + "\nK 13\n"
//                                                                        ,STATIC_SEQUENCE + "\nK 12\n"
//                                                                        ,STATIC_SEQUENCE + "\nK 14\n"
//                                                                        ,STATIC_SEQUENCE
    );
    public static void main(String [] args) throws IOException {
        String path = args[0];
        String newPath = args[1];
        FileOutputStream fileStream = null;

        byte[] contents = Files.readAllBytes(Paths.get(path));
        byte[] filteredContents = contents;

        long start = System.currentTimeMillis();

        for (String sequence : SEARCH_SEQUENCES) {
            contents = filteredContents;
            byte[] searchSequence = sequence.getBytes();
            ByteArrayFilter filter = new ByteArrayFilter(contents);
            filteredContents = filter.filter(searchSequence);
        }
        fileStream = new FileOutputStream(newPath);
        fileStream.write(filteredContents);
        fileStream.close();

        System.out.println("Wrote filtered contents to " + newPath);
        System.out.println("Conversion took " + (System.currentTimeMillis() - start) + " ms");
    }


}
