package svndumpfilter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ByteArrayFilter {

    byte [] contents;

    public ByteArrayFilter(byte[] contents) {
        this.contents = contents;
    }


    public byte [] filter(byte [] sequence) {
        byte[] filteredContents = contents;
        List<Integer> indexes = new ArrayList<>();
        int index = indexOf(contents, sequence, 0);
        int count = 0;
        while (index != -1)
        {
            count++;
            indexes.add(index);
            index = indexOf(contents, sequence, index + sequence.length) ;
        }

        System.out.println("Removing " + count + " matches of sequence:\n" + new String(sequence));

        if (indexes.size() > 0)
        {
            int filteredContentSize = contents.length - indexes.size() * sequence.length;
            filteredContents = new byte[filteredContentSize];
//            System.out.println("Original file size " + contents.length + ", new file size is  " + filteredContentSize + " removing " + indexes.size() + " matches of size " + sequence.length);
            int begin = 0;
            int filteredIndex = 0;

            for (int i = 0; i < indexes.size(); i++)
            {
                int end = indexes.get(i);
                byte [] toCopy = Arrays.copyOfRange(contents, begin, end);
//                System.out.println("Copy file chunk from original: " + toCopy);

                fillBuffer(filteredContents, filteredIndex, toCopy);
                filteredIndex = filteredIndex + toCopy.length;
                begin = end + sequence.length;
            }
            //  last chunk
            fillBuffer(filteredContents, filteredIndex, Arrays.copyOfRange(contents, begin, contents.length));
        }
        else
        {
            return contents;
        }

        return filteredContents;
    }


    private  int indexOf(byte[] array, byte[] target, int begin)
    {
        if (target.length == 0)
        {
            return 0;
        }

        outer:
        for (int i = begin; i < array.length - target.length + 1; i++)
        {
            for (int j = 0; j < target.length; j++)
            {
                if (array[i + j] != target[j])
                {
                    continue outer;
                }
            }
            return i;
        }
        return -1;
    }

    private  void fillBuffer(byte[] target, int startIndex, byte[] source)
    {

        System.out.println("Filling new buffer with: " + source.length +
                            " bytes from index " + startIndex + " to index " + (startIndex + source.length -1));
        for (int i = 0; i < source.length; i++)
        {
            target[startIndex+i] = source[i];
        }
    }
}
